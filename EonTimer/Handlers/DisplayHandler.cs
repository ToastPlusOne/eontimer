﻿using EonTimer.Utilities;
using EonTimer.Utilities.Constants;
using EonTimer.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EonTimer.Handlers
{
	public class DisplayHandler : ITimerEventHandler
	{
		private ITimerMonitor _timeMonitor;

		public void Register(ITimerMonitor timer)
		{
			_timeMonitor = timer;
		}


		public List<Control> CurrentDisplays { get; set; } = new List<Control>();
		public List<Control> NextStageDisplays { get; set; } = new List<Control>();
		public List<Control> StatusDisplays { get; set; } = new List<Control>();
		public List<Control> MinutesBeforeDisplays { get; set; } = new List<Control>();


		public void NotifySetup()
		{
			if (_timeMonitor.Timer.Stages != null)
			{
				if (_timeMonitor.Timer.Stages.Count > 0)
				{
					CurrentDisplays.ForEach(c => GUIHelper.SetControlText(c, FormatTime(_timeMonitor.Timer.Stages[0])));
				}

				if (_timeMonitor.Timer.Stages.Count > 1)
				{
					NextStageDisplays.ForEach(c => GUIHelper.SetControlText(c, FormatTime(_timeMonitor.Timer.Stages[1])));
				}
				else
				{
					NextStageDisplays.ForEach(c => GUIHelper.SetControlText(c, FormatTime(Constants.NullTimeSpan)));
				}
			}

			StatusDisplays.ForEach(c => GUIHelper.SetControlText(c, "Stopped"));
			MinutesBeforeDisplays.ForEach(c => GUIHelper.SetControlText(c, _timeMonitor.Timer.GetMinutesBeforeTarget().ToString()));
		}


		public void NotifyStart()
		{
			StatusDisplays.ForEach(c => GUIHelper.SetControlText(c, "Running"));
		}


		public void NotifyStageStart(Int32 stage)
		{
			var ts = Constants.NullTimeSpan;

			if (_timeMonitor.Timer.Stages.Count > stage + 1)
				ts = _timeMonitor.Timer.Stages[stage + 1];

			NextStageDisplays.ForEach(c => GUIHelper.SetControlText(c, FormatTime(ts)));
		}


		public void NotifyUpdate(TimeSpan remaining)
		{
			CurrentDisplays.ForEach(c => GUIHelper.SetControlText(c, FormatTime(remaining)));
		}


		public void NotifyEnd()
		{
			NotifySetup();
		}


		public void NotifyStageEnd(Int32 stage) { }


		private String FormatTime(TimeSpan ts)
		{
			if (ts == Constants.NullTimeSpan)
				return "0:00";

			if (ts == Constants.InfiniteTimeSpan)
				return "?:??";

			return
				$"{(Int32)ts.TotalMilliseconds / 1000}:{((Int32)ts.TotalMilliseconds / 10 % 100):D2}";
		}
	}
}