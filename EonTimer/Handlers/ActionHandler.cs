﻿using EonTimer.Actions;
using EonTimer.Utilities;
using System;
using System.Collections.Generic;

namespace EonTimer.Handlers
{
	public class ActionHandler : ITimerEventHandler
	{
		private ITimerMonitor _timeMonitor;
		private TimeSpan _nextAction;


		public List<ICountdownAction> Actions = new List<ICountdownAction>();
		public TimeSpan Interval { get; set; }
		public Int32 ActionCount { get; set; }


		public void Register(ITimerMonitor timer)
		{
			_timeMonitor = timer;
		}


		public void NotifyStageStart(Int32 stage)
		{
			var next = new TimeSpan(Interval.Ticks * (ActionCount - 1));
			var stageTime = _timeMonitor.Timer.Stages[stage];


			if (stageTime < next && Math.Abs(Interval.TotalMilliseconds) > 1)
				_nextAction = new TimeSpan(0, 0, 0, 0, (Int32)stageTime.TotalMilliseconds / (Int32)Interval.TotalMilliseconds * (Int32)Interval.TotalMilliseconds); //keeps intervals even
			else
				_nextAction = next;
		}


		public void NotifyUpdate(TimeSpan remaining)
		{
			if (remaining < _nextAction)
			{
				foreach (var action in Actions)
					action.Action();

				_nextAction = _nextAction.Subtract(Interval);
			}
		}


		public void NotifySetup() { }
		public void NotifyStart() { }
		public void NotifyStageEnd(Int32 stage) { }
		public void NotifyEnd() { }
	}
}