﻿using EonTimer.Actions;
using EonTimer.Handlers;
using EonTimer.Properties;
using EonTimer.Timers;
using EonTimer.Utilities;
using EonTimer.Utilities.Constants;
using EonTimer.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Settings = EonTimer.Properties.UserSettings;

namespace EonTimer
{
	public partial class MainTimer : Form, ITimeoutHandler
	{
		private ITimerMonitor _monitor;
		private DisplayHandler _displayHandler;
		private ActionHandler _actionHandler;
		private Boolean _eventLock;
		private EonTimerSettings _settingsForm;
		private List<Int32> _customStages;

		#region Setup
		public MainTimer()
		{
			var isMono = Type.GetType("Mono.Runtime") != null;

			//migrates settings if necessary
			if (!isMono)
				RegistryMigrationHelper.Migrate();

			//Build form
			InitializeComponent();
			InitializeCustom();
			PrepareTimeMonitor();
			ShowSettingsInControls();
			ApplyOpacity();
			HideGuiElements();

			CreateTimer();
			SetMini(Settings.Default.Setting_Form_Mini);
			SetPin();
		}


		private void InitializeCustom()
		{
			//attach handlers
			MouseDown += Form_MouseDown;
			MouseUp += Form_MouseUp;
			MouseMove += Form_MouseMove;

			//set images
			pictureClose.Image = _closeButton.Basic;
			pictureMinimize.Image = _minimizeButton.Basic;
			pictureMini.Image = Settings.Default.Setting_Form_Mini ? _miniButton.Active : _miniButton.Basic;
			picturePin.Image = _pinButton.Basic;
			pictureSettings.Image = _settingsButton.Basic;

			//fill combo boxes
			combo_mode_5.Items.AddRange(Generations.Five.ModeStrings);
			combo_mode_4.Items.AddRange(Generations.Four.ModeStrings);
			combo_mode_3.Items.AddRange(Generations.Three.ModeStrings);

			_settingsForm = new EonTimerSettings { TransparencyHandler = UpdateOpacity };
			_settingsForm.FormClosing += OnSettingsFormClosing;

			_customStages = new List<Int32>();

			var ctrls = new List<Control>();
			var g = CreateGraphics();
			var conversionRate = 96 / g.DpiX;
			GetAllControls(this, ctrls);
			foreach (var ctrl in ctrls)
			{
				ctrl.Font = new Font(ctrl.Font.FontFamily, ctrl.Font.Size * conversionRate);
			}
		}


		private void GetAllControls(Control container, IList<Control> outList)
		{
			foreach (Control ctrl in container.Controls)
			{
				if (ctrl.Controls.Count == 0)
					outList.Add(ctrl);
				else
					GetAllControls(ctrl, outList);
			}
		}


		private void PrepareTimeMonitor()
		{
			_monitor = new TimeMonitor(new NullTimer(), Settings.Default.Setting_Timer_SleepInterval);

			//create DisplayHandler
			_displayHandler = new DisplayHandler();
			_displayHandler.CurrentDisplays.Add(displayCurrent);
			_displayHandler.NextStageDisplays.Add(displayNextStage);
			_displayHandler.MinutesBeforeDisplays.Add(displayMinsBefore);

			//create ActionHandler
			_actionHandler = new ActionHandler
			{
				ActionCount = Settings.Default.Setting_Action_Count,
				Interval = new TimeSpan(0, 0, 0, 0, Settings.Default.Setting_Action_Interval)
			};
			SetHandlers();
		}
		#endregion


		#region Update Methods
		private void SetHandlers()
		{
			CreateCountdownActions();
			_monitor.ClearHandlers();
			_monitor.AddHandler(_actionHandler);
			_monitor.AddHandler(_displayHandler);
			_monitor.AddHandler(this);
		}


		private void CreateCountdownActions()
		{
			_actionHandler.Actions.Clear();

			switch ((Alerts.ActionType)Settings.Default.Setting_Action_Mode)
			{
				case Alerts.ActionType.Audio:
					_actionHandler.Actions.Add(GetSoundAction());
					break;
				case Alerts.ActionType.Visual:
					_actionHandler.Actions.Add(GetVisualAction());
					break;
				case Alerts.ActionType.Dual:
					_actionHandler.Actions.Add(GetSoundAction());
					_actionHandler.Actions.Add(GetVisualAction());
					break;
			}
		}


		private SoundAction GetSoundAction()
		{
			switch ((Alerts.SoundType)Settings.Default.Setting_Action_Sound)
			{
				case Alerts.SoundType.Beep:
					return new SoundAction(Resources.beep);
				case Alerts.SoundType.Ding:
					return new SoundAction(Resources.ding);
				case Alerts.SoundType.Pop:
					return new SoundAction(Resources.pop);
				case Alerts.SoundType.Tick:
					return new SoundAction(Resources.tick);
				default:
					return new SoundAction();
			}
		}


		private VisualAction GetVisualAction()
		{
			return new VisualAction(displayCurrent, Settings.Default.Setting_Action_Color);
		}


		public void ShowSettingsInControls()
		{
			if (_eventLock)
				return;

			_eventLock = true;

			//gen 5 settings
			combo_mode_5.SelectedIndex = UserData.Default.Mode_5;
			text_calibration_5.Text = UserData.Default.Calibration_5_Basic.ToString();
			text_target_delay_5.Text = UserData.Default.Target_5_Delay.ToString();
			text_target_second_5.Text = UserData.Default.Target_5_Second.ToString();
			text_calibration_entralink_5.Text = UserData.Default.Calibration_5_Entralink.ToString();
			text_calibration_5_frame.Text = UserData.Default.Calibration_5_Frame.ToString();
			text_target_5_frame.Text = UserData.Default.Target_5_EntralinkFrame.ToString();

			//gen 4 settings
			combo_mode_4.SelectedIndex = UserData.Default.Mode_4;
			text_calibration_delay_4.Text = UserData.Default.Calibration_4_Delay.ToString();
			text_calibration_second_4.Text = UserData.Default.Calibration_4_Second.ToString();
			text_target_delay_4.Text = UserData.Default.Target_4_Delay.ToString();
			text_target_second_4.Text = UserData.Default.Target_4_Second.ToString();

			//gen 3 settings
			combo_mode_3.SelectedIndex = UserData.Default.Mode_3;
			text_calibration_lag_3.Text = UserData.Default.Calibration_3_Lag.ToString();
			text_calibration_factor_3.Text = UserData.Default.Calibration_3_Factor.ToString(CultureInfo.InvariantCulture);
			text_target_initial_3.Text = UserData.Default.Target_3_Initial.ToString();
			text_target_frame_3.Text = UserData.Default.Target_3_Frame.ToString();

			_eventLock = false;
		}


		public void UpdateSettingsFromControls()
		{
			if (_eventLock)
				return;

			_eventLock = true;

			//gen 5 settings
			UserData.Default.Mode_5 = combo_mode_5.SelectedIndex;
			UserData.Default.Calibration_5_Basic = SetInt(text_calibration_5.Text, UserData.Default.Calibration_5_Basic);
			UserData.Default.Target_5_Delay = SetInt(text_target_delay_5.Text, UserData.Default.Target_5_Delay);
			UserData.Default.Target_5_Second = SetInt(text_target_second_5.Text, UserData.Default.Target_5_Second);
			UserData.Default.Calibration_5_Entralink = SetInt(text_calibration_entralink_5.Text, UserData.Default.Calibration_5_Entralink);
			UserData.Default.Calibration_5_Frame = SetInt(text_calibration_5_frame.Text, UserData.Default.Calibration_5_Frame);
			UserData.Default.Target_5_EntralinkFrame = SetInt(text_target_5_frame.Text, UserData.Default.Target_5_EntralinkFrame);

			//gen 4 settings
			UserData.Default.Mode_4 = combo_mode_4.SelectedIndex;
			UserData.Default.Calibration_4_Delay = SetInt(text_calibration_delay_4.Text, UserData.Default.Calibration_4_Delay);
			UserData.Default.Calibration_4_Second = SetInt(text_calibration_second_4.Text, UserData.Default.Calibration_4_Second);
			UserData.Default.Target_4_Delay = SetInt(text_target_delay_4.Text, UserData.Default.Target_4_Delay);
			UserData.Default.Target_4_Second = SetInt(text_target_second_4.Text, UserData.Default.Target_4_Second);

			//gen 3 settings
			UserData.Default.Mode_3 = combo_mode_3.SelectedIndex;
			UserData.Default.Calibration_3_Lag = SetInt(text_calibration_lag_3.Text, UserData.Default.Calibration_3_Lag);
			UserData.Default.Calibration_3_Factor = SetDecimal(text_calibration_factor_3.Text, UserData.Default.Calibration_3_Factor);
			UserData.Default.Target_3_Initial = SetInt(text_target_initial_3.Text, UserData.Default.Target_3_Initial);
			UserData.Default.Target_3_Frame = SetInt(text_target_frame_3.Text, UserData.Default.Target_3_Frame);

			_eventLock = false;
		}


		private Int32 SetInt(String toParse, Int32 defaultResult)
		{
			if (!String.IsNullOrEmpty(toParse) && Int32.TryParse(toParse, out var result))
				return result;

			return defaultResult;
		}


		private Decimal SetDecimal(String toParse, Decimal defaultResult)
		{
			if (Decimal.TryParse(toParse, out var result))
				return result;

			return defaultResult;
		}


		private void CreateTimer()
		{
			if (_monitor.IsRunning() || _eventLock)
				return;

			var type = (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console;
			var min = Settings.Default.Setting_Timer_Minimum;

			// Gen 5
			if (tabMenu.SelectedTab.Equals(tabGen5))
			{
				var calibration = Settings.Default.Setting_Timer_PreciseCalibration ? UserData.Default.Calibration_5_Basic : CalibrationHelper.ConvertToMillis(UserData.Default.Calibration_5_Basic, type);
				var elCal = Settings.Default.Setting_Timer_PreciseCalibration ? UserData.Default.Calibration_5_Entralink : CalibrationHelper.ConvertToMillis(UserData.Default.Calibration_5_Entralink, type);

				switch ((Generations.Five.Modes)UserData.Default.Mode_5)
				{
					case Generations.Five.Modes.Standard:
						_monitor.Timer = new SimpleTimer(calibration, UserData.Default.Target_5_Second, type, min);
						break;
					case Generations.Five.Modes.CGear:
						_monitor.Timer = new DelayTimer(calibration, UserData.Default.Target_5_Delay, UserData.Default.Target_5_Second, type, min);
						break;
					case Generations.Five.Modes.Entralink:
						_monitor.Timer = new EntralinkTimer(calibration, elCal, UserData.Default.Target_5_Delay, UserData.Default.Target_5_Second, type, min);
						break;
					case Generations.Five.Modes.EntralinkPlus:
						_monitor.Timer = new EnhancedEntralinkTimer(calibration, elCal, UserData.Default.Target_5_Delay, UserData.Default.Target_5_Second, UserData.Default.Target_5_EntralinkFrame, UserData.Default.Calibration_5_Frame, type, min);
						break;
					default:
						_monitor.Timer = new NullTimer();
						break;
				}
			} // Gen 4
			else if (tabMenu.SelectedTab.Equals(tabGen4))
			{
				switch ((Generations.Four.Modes)UserData.Default.Mode_4)
				{
					case Generations.Four.Modes.Standard:
						_monitor.Timer = new DelayTimer(CalibrationHelper.CreateCalibration(UserData.Default.Calibration_4_Delay, UserData.Default.Calibration_4_Second, type), UserData.Default.Target_4_Delay, UserData.Default.Target_4_Second, type, min);
						break;
					default:
						_monitor.Timer = new NullTimer();
						break;
				}
			} // Gen 3
			else if (tabMenu.SelectedTab.Equals(tabGen3))
			{
				switch ((Generations.Three.Modes)UserData.Default.Mode_3)
				{
					case Generations.Three.Modes.Standard:
						_monitor.Timer = new FrameTimer(UserData.Default.Calibration_3_Lag, UserData.Default.Target_3_Initial, UserData.Default.Target_3_Frame, type);
						break;
					case Generations.Three.Modes.VariableTarget:
						_monitor.Timer = new VariableTargetFrameTimer(type);
						break;
					default:
						_monitor.Timer = new NullTimer();
						break;
				}
			}
			else if (tabMenu.SelectedTab.Equals(tabCustom))
			{
				_monitor.Timer = new CustomTimer(_customStages.ToArray());
			}
		}


		private void ApplyOpacity()
		{
			Opacity = Settings.Default.Setting_Form_Opacity / 100.0;

		}


		private void Save()
		{
			UserData.Default.Save();
			Settings.Default.Save();
		}
		#endregion


		#region Form Change Events
		private void NumericKeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar) || e.KeyChar == '-'))
				e.Handled = true;
		}


		private void UpdateTimer(object sender, EventArgs e)
		{
			if (!_eventLock)
			{
				UpdateSettingsFromControls();
				HideGuiElements();
				CreateTimer();
			}
		}


		private void UpdateOpacity(object sender, EventArgs e)
		{
			ApplyOpacity();
		}


		private void HideGuiElements()
		{
			labelDelayHit5.Text = "Delay Hit";

			if (tabMenu.SelectedTab.Equals(tabGen5))
			{
				switch ((Generations.Five.Modes)UserData.Default.Mode_5)
				{
					case Generations.Five.Modes.Standard:
						Hide(new Control[] { label_target_delay_5, text_target_delay_5, label_calibration_entralink_5, text_calibration_entralink_5, text_hit_5_second, label_hit_5_second, panel_enhancedEL });
						labelDelayHit5.Text = "Second Hit";
						break;
					case Generations.Five.Modes.CGear:
						Hide(new Control[] { label_calibration_entralink_5, text_calibration_entralink_5, text_hit_5_second, label_hit_5_second, panel_enhancedEL });
						Show(new Control[] { label_target_delay_5, text_target_delay_5 });
						break;
					case Generations.Five.Modes.Entralink:
						Show(new Control[] { label_target_delay_5, text_target_delay_5, label_calibration_entralink_5, text_calibration_entralink_5, text_hit_5_second, label_hit_5_second });
						Hide(new Control[] { panel_enhancedEL });
						break;
					case Generations.Five.Modes.EntralinkPlus:
						Show(new Control[] { label_target_delay_5, text_target_delay_5, label_calibration_entralink_5, text_calibration_entralink_5, panel_enhancedEL, text_hit_5_second, label_hit_5_second, panel_enhancedEL });
						break;
				}
			}
			else if (tabMenu.SelectedTab.Equals(tabGen4))
			{
				Hide(new Control[] { panel_enhancedEL });
			}
			else if (tabMenu.SelectedTab.Equals(tabGen3))
			{
				switch ((Generations.Three.Modes)UserData.Default.Mode_3)
				{
					case Generations.Three.Modes.Standard:
						Show(new Control[] { text_hit_3, labelHit3, label_calibration_lag_3, text_calibration_lag_3 });
						Hide(new Control[] { panel_enhancedEL });
						break;
					case Generations.Three.Modes.VariableTarget:
						Hide(new Control[] { text_hit_3, labelHit3, label_calibration_lag_3, text_calibration_lag_3, panel_enhancedEL });
						break;
				}
			}
		}


		private void Hide(Control[] ctrls)
		{
			foreach (var ctrl in ctrls)
				ctrl.Visible = false;
		}


		private void Show(Control[] ctrls)
		{
			foreach (var ctrl in ctrls)
				ctrl.Visible = true;
		}
		#endregion


		private void Start(object sender, EventArgs e)
		{
			if (!_monitor.IsRunning())
			{
				buttonStart.Text = "Cancel";
				CreateTimer();
				_monitor.Run();
			}
			else
			{
				_monitor.Cancel();
				CreateTimer();
			}
		}


		#region Form Control Events
		/// <summary>Updates Calibration</summary>
		private void UpdateCalibration(object sender, EventArgs e)
		{
			var temp = 0;

			if (tabMenu.SelectedTab.Equals(tabGen5))
			{
				var offsetAccounted = 0;

				switch ((Generations.Five.Modes)UserData.Default.Mode_5)
				{
					case Generations.Five.Modes.Standard:
						temp = ((SimpleTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_delay.Text, UserData.Default.Target_5_Second));
						UserData.Default.Calibration_5_Basic += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						break;
					case Generations.Five.Modes.CGear:
						temp = ((DelayTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_delay.Text, UserData.Default.Target_5_Delay));
						UserData.Default.Calibration_5_Basic += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						break;
					case Generations.Five.Modes.Entralink:
						temp = ((SimpleTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_second.Text, UserData.Default.Target_5_Second));

						offsetAccounted = temp;
						UserData.Default.Calibration_5_Basic += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						temp = ((EntralinkTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_delay.Text, UserData.Default.Target_5_Delay) - offsetAccounted);

						UserData.Default.Calibration_5_Entralink += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						break;
					case Generations.Five.Modes.EntralinkPlus:
						temp = ((SimpleTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_second.Text, UserData.Default.Target_5_Second));

						offsetAccounted = temp;
						UserData.Default.Calibration_5_Basic += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						temp = ((EntralinkTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_delay.Text, UserData.Default.Target_5_Delay) - offsetAccounted);

						UserData.Default.Calibration_5_Entralink += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						temp = ((EnhancedEntralinkTimer)_monitor.Timer).Calibrate(SetInt(text_hit_5_frame.Text, UserData.Default.Target_5_EntralinkFrame));

						UserData.Default.Calibration_5_Frame += temp;
						break;
				}

				text_hit_5_frame.Text = "";
				text_hit_5_second.Text = "";
				text_hit_5_delay.Text = "";
			}
			else if (tabMenu.SelectedTab.Equals(tabGen4))
			{
				switch ((Generations.Four.Modes)UserData.Default.Mode_4)
				{
					case Generations.Four.Modes.Standard:
						temp = ((DelayTimer)_monitor.Timer).Calibrate(SetInt(text_hit_4.Text, UserData.Default.Calibration_4_Delay));
						UserData.Default.Calibration_4_Delay += Settings.Default.Setting_Timer_PreciseCalibration ? temp : CalibrationHelper.ConvertToDelays(temp, (Consoles.ConsoleType)Settings.Default.Setting_Timer_Console);
						break;
				}

				text_hit_4.Text = "";
			}
			else if (tabMenu.SelectedTab.Equals(tabGen3))
			{
				switch ((Generations.Three.Modes)UserData.Default.Mode_3)
				{
					case Generations.Three.Modes.Standard:
						temp = ((FrameTimer)_monitor.Timer).Calibrate(SetInt(text_hit_3.Text, UserData.Default.Calibration_3_Lag));
						UserData.Default.Calibration_3_Lag += temp;
						break;
					case Generations.Three.Modes.VariableTarget:
						temp = SetInt(text_target_frame_3.Text, 10000);
						if (_monitor.Timer is VariableTargetFrameTimer timer)
							timer.TargetFrame = temp;
						break;
				}

				text_hit_3.Text = "";
			}

			ShowSettingsInControls();
		}


		private void Save(object sender, EventArgs e) => Save();
		private void Close(object sender, EventArgs e) => Close();
		private void Minimize(object sender, EventArgs e) => WindowState = FormWindowState.Minimized;


		private void Mini(object sender, EventArgs e)
		{
			Settings.Default.Setting_Form_Mini = !Settings.Default.Setting_Form_Mini;
			SetMini(Settings.Default.Setting_Form_Mini);
		}


		private void SetMini(Boolean mini)
		{
			//hide interface
			tabMenu.Visible = !mini;
			buttonUpdate.Visible = !mini;
			buttonSave.Visible = !mini;

			//form changes
			if (mini)
			{
				BackgroundImage = Resources.glaceonmini;
				Height = 148;
				Width = 246;
				pictureMini.Image = _miniButton.Active;
			}
			else
			{
				BackgroundImage = Resources.glaceonbg;
				Height = 293;
				Width = 392;
				pictureMini.Image = _miniButton.Basic;
			}
		}


		private void Pin(object sender, EventArgs e)
		{
			Settings.Default.Setting_Form_OnTop = !Settings.Default.Setting_Form_OnTop;
			SetPin();
		}


		private void SetPin()
		{
			TopMost = Settings.Default.Setting_Form_OnTop;
			picturePin.Image = _pinButton.Active;
		}


		private void OpenSettings(object sender, EventArgs e)
		{
			_settingsForm.Show();
		}
		#endregion


		#region Other Form Events
		private void OnClosing(object sender, FormClosingEventArgs e)
		{
			_monitor.Cancel();

			if (Settings.Default.Setting_Form_AutoSave)
				Save();
			else if (Settings.Default.Setting_Form_AskSave)
			{
				var result = MessageBox.Show("Store input?", "Save values?", MessageBoxButtons.YesNoCancel);

				if (result == DialogResult.Yes)
					Save();
				else if (result == DialogResult.Cancel)
					e.Cancel = true;
			}
		}


		private void OnSettingsFormClosing(object sender, FormClosingEventArgs e)
		{
			_settingsForm.Hide();
			Settings.Default.Reload();
			ApplyOpacity();
			CreateCountdownActions();
			_actionHandler.Interval = new TimeSpan(0, 0, 0, 0, Settings.Default.Setting_Action_Interval);
			_actionHandler.ActionCount = Settings.Default.Setting_Action_Count;
			CreateTimer();
			e.Cancel = true;
		}
		#endregion


		#region custom timer
		private void Custom_AddStage(object sender, EventArgs e)
		{
			if (Int32.TryParse(text_custom_add.Text, out var temp))
			{
				list_custom.Items.Add(temp);
				text_custom_add.Text = "";
				ApplyStages();
				CreateTimer();
			}
			else
				MessageBox.Show("Invalid. Please enter an integer.");
		}


		private void Custom_RemoveStage(object sender, EventArgs e)
		{
			if (list_custom.SelectedIndex != -1)
				list_custom.Items.RemoveAt(list_custom.SelectedIndex);

			CreateTimer();
		}


		private void Custom_ClearAll(object sender, EventArgs e)
		{
			list_custom.Items.Clear();
			_customStages.Clear();
			CreateTimer();
		}


		private void Custom_Edit(object sender, EventArgs e)
		{
			var index = list_custom.SelectedIndex;

			if (Int32.TryParse(text_custom_edit.Text, out var temp))
			{
				list_custom.Items.Insert(index, temp);
				list_custom.Items.RemoveAt(index + 1);
				text_custom_edit.Text = "";
				ApplyStages();
				CreateTimer();
			}
			else
				MessageBox.Show("Invalid. Please enteran integer.");
		}


		private void Custom_Selected(object sender, EventArgs e)
		{
			try
			{
				text_custom_edit.Text = list_custom.SelectedItem.ToString();
			}
			catch (NullReferenceException)
			{
				//this can happen when an entry is removed
			}
		}


		private void ApplyStages()
		{
			_customStages.Clear();
			foreach (var item in list_custom.Items)
			{
				_customStages.Add(Int32.Parse(item.ToString()));
			}
		}
		#endregion


		#region Image Button Support
		private class EonButton
		{
			public Image Basic { get; set; }
			public Image Hover { get; set; }
			public Image Active { get; set; }
		}


		private readonly EonButton _minimizeButton = new EonButton
		{
			Basic = Resources.Minimize,
			Hover = Resources.Minimize_Hover
		};


		private readonly EonButton _closeButton = new EonButton
		{
			Basic = Resources.Close,
			Hover = Resources.Close_Hover
		};


		private readonly EonButton _settingsButton = new EonButton
		{
			Basic = Resources.Settings,
			Hover = Resources.Settings_Hover
		};


		private readonly EonButton _miniButton = new EonButton
		{
			Basic = Resources.Mini,
			Hover = Resources.Mini_Hover,
			Active = Resources.Mini_Active
		};


		private readonly EonButton _pinButton = new EonButton
		{
			Basic = Resources.Pin,
			Hover = Resources.Pin_Hover,
			Active = Resources.Pin_Active
		};


		private void Close_MouseEnter(object sender, EventArgs e)
		{
			pictureClose.Image = _closeButton.Hover;
		}


		private void Close_MouseLeave(object sender, EventArgs e)
		{
			pictureClose.Image = _closeButton.Basic;
		}


		private void Minimize_MouseEnter(object sender, EventArgs e)
		{
			pictureMinimize.Image = _minimizeButton.Hover;
		}


		private void Minimize_MouseLeave(object sender, EventArgs e)
		{
			pictureMinimize.Image = _minimizeButton.Basic;
		}


		private void Mini_MouseEnter(object sender, EventArgs e)
		{
			pictureMini.Image = _miniButton.Hover;
		}
		private void Mini_MouseLeave(object sender, EventArgs e)
		{
			pictureMini.Image = Settings.Default.Setting_Form_Mini ? _miniButton.Active : _miniButton.Basic;
		}


		private void Pin_MouseEnter(object sender, EventArgs e)
		{
			picturePin.Image = _pinButton.Hover;
		}


		private void Pin_MouseLeave(object sender, EventArgs e)
		{
			picturePin.Image = Settings.Default.Setting_Form_OnTop ? _pinButton.Active : _pinButton.Basic;
		}


		private void Settings_MouseEnter(object sender, EventArgs e)
		{
			pictureSettings.Image = _settingsButton.Hover;
		}


		private void Settings_MouseLeave(object sender, EventArgs e)
		{
			pictureSettings.Image = _settingsButton.Basic;
		}
		#endregion

		#region Form Drag Support

		private Boolean _drag;
		private Point _startPoint = new Point(0, 0);

		protected override void OnControlAdded(ControlEventArgs e)
		{
			e.Control.MouseDown += Form_MouseDown;
			e.Control.MouseUp += Form_MouseUp;
			e.Control.MouseMove += Form_MouseMove;
			base.OnControlAdded(e);
		}


		private void Form_MouseDown(object sender, MouseEventArgs e)
		{
			_drag = true;
			_startPoint = new Point(e.X, e.Y);
		}


		private void Form_MouseUp(object sender, MouseEventArgs e)
		{
			_drag = false;
		}


		private void Form_MouseMove(object sender, MouseEventArgs e)
		{
			if (_drag)
			{
				var p1 = new Point(e.X, e.Y);
				var p2 = PointToScreen(p1);
				var p3 = new Point(p2.X - _startPoint.X,
									 p2.Y - _startPoint.Y);
				Location = p3;
			}
		}
		#endregion


		public void NotifyTimeout()
		{
			GUIHelper.SetControlText(buttonStart, "Start");
			CreateTimer();
		}
	}
}