﻿using EonTimer.Utilities.Constants;
using System;
using System.Windows.Forms;
using Settings = EonTimer.Properties.UserSettings;

namespace EonTimer
{
	public partial class EonTimerSettings : Form
	{
		private Boolean _eventLock;

		public EonTimerSettings()
		{
			InitializeComponent();
			InitializeCustom();
			ShowSettingsInControls();
		}

		public EventHandler TransparencyHandler { set => trackOpacity.Scroll += value; }


		private void InitializeCustom()
		{
			foreach (var item in Utilities.Constants.Alerts.Types)
				combo_setting_actionmode.Items.Add(item);
			foreach (var item in Utilities.Constants.Alerts.Sounds)
				combo_setting_sound.Items.Add(item);
			foreach (var item in Consoles.All)
				combo_setting_console.Items.Add(item);

			trackOpacity.Scroll += Change;
		}


		public void ShowSettingsInControls()
		{
			if (_eventLock)
				return;

			_eventLock = true;

			//Actions
			combo_setting_actionmode.SelectedIndex = Settings.Default.Setting_Action_Mode;
			combo_setting_sound.SelectedIndex = Settings.Default.Setting_Action_Sound;
			panel_setting_color.BackColor = Settings.Default.Setting_Action_Color;
			text_setting_frequency.Text = Settings.Default.Setting_Action_Interval.ToString();
			text_setting_count.Text = Settings.Default.Setting_Action_Count.ToString();

			//App
			if (Settings.Default.Setting_Form_AskSave)
				combo_setting_onexit.SelectedIndex = 0;
			else if (Settings.Default.Setting_Form_AutoSave)
				combo_setting_onexit.SelectedIndex = 1;
			else
				combo_setting_onexit.SelectedIndex = 2;
			trackOpacity.Value = Settings.Default.Setting_Form_Opacity;
			check_settings_updates.Checked = Settings.Default.Setting_Settings_Updates;

			//Timer
			combo_setting_console.SelectedIndex = Settings.Default.Setting_Timer_Console;
			check_setting_precisionmode.Checked = Settings.Default.Setting_Timer_PreciseCalibration;
			text_setting_refresh.Value = Settings.Default.Setting_Timer_SleepInterval;

			_eventLock = false;
		}


		public void ApplySettingsFromControls()
		{
			if (_eventLock)
				return;

			_eventLock = true;

			//Actions
			Settings.Default.Setting_Action_Mode = combo_setting_actionmode.SelectedIndex;
			Settings.Default.Setting_Action_Sound = combo_setting_sound.SelectedIndex;
			Settings.Default.Setting_Action_Color = panel_setting_color.BackColor;
			Settings.Default.Setting_Action_Interval = SetInt(text_setting_frequency.Text, Settings.Default.Setting_Action_Interval);
			Settings.Default.Setting_Action_Count = SetInt(text_setting_count.Text, Settings.Default.Setting_Action_Count);

			//App
			Settings.Default.Setting_Form_AskSave = false;
			Settings.Default.Setting_Form_AutoSave = false;
			switch (combo_setting_onexit.SelectedIndex)
			{
				case 0:
					Settings.Default.Setting_Form_AskSave = true;
					break;
				case 1:
					Settings.Default.Setting_Form_AutoSave = true;
					break;
			}
			Settings.Default.Setting_Form_Opacity = trackOpacity.Value;
			Settings.Default.Setting_Settings_Updates = check_settings_updates.Checked;

			//Timer
			Settings.Default.Setting_Timer_Console = combo_setting_console.SelectedIndex;
			Settings.Default.Setting_Timer_PreciseCalibration = check_setting_precisionmode.Checked;
			Settings.Default.Setting_Timer_SleepInterval = (Int32)text_setting_refresh.Value;

			_eventLock = false;
		}

		private Int32 SetInt(String toParse, Int32 defaultResult)
		{
			return Int32.TryParse(toParse, out var result) ? result : defaultResult;
		}

		private void Save(Object sender, EventArgs e)
		{
			Settings.Default.Save();
			Close();
		}

		private void Change(Object sender, EventArgs e)
		{
			ApplySettingsFromControls();
		}

		private void SelectColor(Object sender, EventArgs e)
		{
			var colorDialog = new ColorDialog();

			if (colorDialog.ShowDialog() == DialogResult.OK)
				Settings.Default.Setting_Action_Color = colorDialog.Color;

			panel_setting_color.BackColor = Settings.Default.Setting_Action_Color;
		}

		private void NumericKeyPress(Object sender, KeyPressEventArgs e)
		{
			if (!(Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar) || e.KeyChar == '-'))
				e.Handled = true;
		}
	}
}
