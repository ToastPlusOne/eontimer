﻿using EonTimer.Utilities.Constants;
using System;

namespace EonTimer.Utilities.Helpers
{
	public static class CalibrationHelper
	{
		public static Int32 ConvertToMillis(Int32 delays, Consoles.ConsoleType type)
		{
			switch (type)
			{
				case Consoles.ConsoleType.GBA:
					return (Int32)Math.Round(delays * FrameRates.GBA);
				case Consoles.ConsoleType.NDS:
					return (Int32)Math.Round(delays * FrameRates.NDS);
				case Consoles.ConsoleType.DSI:
					return (Int32)Math.Round(delays * FrameRates.DSI);
				case Consoles.ConsoleType.N3DS:
					return (Int32)Math.Round(delays * FrameRates.N3DS);
				default:
					return 0;
			}
		}


		public static Int32 ConvertToDelays(Int32 millis, Consoles.ConsoleType type)
		{
			switch (type)
			{
				case Consoles.ConsoleType.GBA:
					return (Int32)Math.Round(millis / FrameRates.GBA);
				case Consoles.ConsoleType.NDS:
					return (Int32)Math.Round(millis / FrameRates.NDS);
				case Consoles.ConsoleType.DSI:
					return (Int32)Math.Round(millis / FrameRates.DSI);
				case Consoles.ConsoleType.N3DS:
					return (Int32)Math.Round(millis / FrameRates.N3DS);
				default:
					return 0;
			}
		}


		public static Int32 CreateCalibration(Int32 delay, Int32 second, Consoles.ConsoleType type)
		{
			var delayCalibration = delay - ConvertToDelays(second * 1000, type);
			return ConvertToMillis(delayCalibration, type);
		}
	}
}