﻿using System;

namespace EonTimer.Utilities.Constants
{
	public static class Alerts
	{
		public enum ActionType { Audio, Visual, Dual, None };
		public static readonly String[] Types = Enum.GetNames(typeof(ActionType));

		public enum SoundType { Beep, Ding, Pop, Tick };
		public static readonly String[] Sounds = Enum.GetNames(typeof(SoundType));
	}
}
