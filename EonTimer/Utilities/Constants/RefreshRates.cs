﻿using System;

namespace EonTimer.Utilities.Constants {
	public static class RefreshRates
	{
		public const Decimal GBA = 59.7271M;
		public const Decimal NDS = 59.8261M;
		public const Decimal DSI = 59.8261M;
		public const Decimal N3DS = 59.8261M;
	}
}