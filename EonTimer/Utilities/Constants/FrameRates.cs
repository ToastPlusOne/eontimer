﻿using System;

namespace EonTimer.Utilities.Constants {
	public static class FrameRates
	{
		public const Decimal GBA = 1000 / RefreshRates.GBA;
		public const Decimal NDS = 1000 / RefreshRates.NDS;
		public const Decimal DSI = 1000 / RefreshRates.DSI;
		public const Decimal N3DS = 1000 / RefreshRates.N3DS;
	}
}