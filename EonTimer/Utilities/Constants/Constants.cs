﻿using System;

namespace EonTimer.Utilities.Constants
{
	public static class Constants
	{
		public const Decimal UpdateFactor = 1;
		public const Decimal CloseUpdateFactor = .75M;
		public const Int32 CloseThreshold = 167;

		public static readonly TimeSpan NullTimeSpan = new TimeSpan(-999);
		public static readonly TimeSpan InfiniteTimeSpan = new TimeSpan(-99);

		public const Decimal EntralinkRate = 0.837148929M;
	}
}