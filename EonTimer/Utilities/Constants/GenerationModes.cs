﻿using System;

namespace EonTimer.Utilities.Constants
{
	public static class Generations
	{
		public static class Five
		{
			public enum Modes
			{
				Standard,
				CGear,
				Entralink,
				EntralinkPlus
			};

			public static readonly String[] ModeStrings = { "Standard", "C-Gear", "Entralink", "Entralink+" };
		}

		public static class Four
		{
			public enum Modes
			{
				Standard
			};

			public static readonly String[] ModeStrings = { "Standard" };
		}

		public static class Three
		{
			public enum Modes { Standard, VariableTarget };
			public static readonly String[] ModeStrings = { "Standard", "Variable Target" };
		}
	}
}