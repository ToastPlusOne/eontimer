﻿using System;

namespace EonTimer.Utilities.Constants
{
	public static class Consoles
	{
		public const String GBA = "GBA";
		public const String NDS = "NDS";
		public const String DSI = "DSI";
		public const String N3DS = "3DS";

		public static readonly String[] All = { GBA, NDS, DSI, N3DS };

		public enum ConsoleType { GBA, NDS, DSI, N3DS }
	}
}