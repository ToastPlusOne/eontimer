﻿namespace EonTimer.Utilities
{
    public interface ITimeoutHandler
    {
        void NotifyTimeout();
    }
}
