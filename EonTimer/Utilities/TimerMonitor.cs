﻿using EonTimer.Handlers;
using EonTimer.Timers;
using System;
using System.Collections.Generic;
using System.Threading;

namespace EonTimer.Utilities
{
	public class TimeMonitor : ITimerMonitor
	{
		private Thread _thread;
		private ITimer _timer;

		public TimeMonitor(ITimer timer, Int32 sleepInterval = 8)
		{
			Timer = timer;
			SleepInterval = sleepInterval;
		}


		public List<ITimerEventHandler> Handlers { get; } = new List<ITimerEventHandler>();
		public List<ITimeoutHandler> TimeoutHandlers { get; } = new List<ITimeoutHandler>();
		public Int32 SleepInterval { get; set; }
		public ITimer Timer
		{
			get => _timer;
			set
			{
				_timer = value;
				foreach (var handler in Handlers)
					handler.NotifySetup();
			}
		}


		public void Run()
		{
			if (_thread == null || !_thread.IsAlive)
			{
				_thread = new Thread(RunTimer);
				_thread.Start();
			}
		}


		private void RunTimer()
		{
			foreach (var h in Handlers)
				h.NotifyStart();

			for (var stage = 0; stage < Timer.Stages.Count; stage++)
				RunStage(stage);

			(Timer as IVariableTimer)?.Reset();

			foreach (var h in Handlers)
				h.NotifyEnd();

			foreach (var handler in TimeoutHandlers)
				handler.NotifyTimeout();
		}


		private void RunStage(Int32 stage)
		{
			var start = DateTime.Now;

			PauseWhileInfinite(stage);

			//stage length is committed at this point
			var endTime = start.Add(Timer.Stages[stage]);
			var remaining = (endTime - DateTime.Now);

			foreach (var h in Handlers)
				h.NotifyStageStart(stage);

			//main timer loop
			while (remaining.TotalMilliseconds > 0)
			{
				Thread.Sleep(SleepInterval);
				remaining = (endTime - DateTime.Now);

				foreach (var h in Handlers)
					h.NotifyUpdate(remaining);
			}

			foreach (var h in Handlers)
				h.NotifyStageEnd(stage);
		}


		public void Cancel()
		{
			if (IsRunning())
				_thread.Abort();

			//wait to update display until thread stops
			while (IsRunning()) { }

			foreach (var handler in Handlers)
				handler.NotifyEnd();
			foreach (var handler in TimeoutHandlers)
				handler.NotifyTimeout();
		}


		public Boolean IsRunning()
		{
			return (_thread != null && _thread.IsAlive);
		}


		public void AddHandler(ITimerEventHandler handler)
		{
			handler.Register(this);
			Handlers.Add(handler);
		}


		public void AddHandler(ITimeoutHandler handler)
		{
			TimeoutHandlers.Add(handler);
		}


		public void ClearHandlers()
		{
			Handlers.Clear();
			TimeoutHandlers.Clear();
		}

		//Acts as a pause for variable-target timers
		private void PauseWhileInfinite(Int32 stage)
		{
			while (Timer.Stages[stage] == Constants.Constants.InfiniteTimeSpan)
				Thread.Sleep(SleepInterval);
		}
	}
}