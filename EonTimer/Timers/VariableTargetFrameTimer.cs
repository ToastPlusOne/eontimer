﻿using EonTimer.Utilities.Constants;
using EonTimer.Utilities.Helpers;
using System;
using System.Collections.Generic;

namespace EonTimer.Timers
{
	public class VariableTargetFrameTimer : ITimer, IVariableTimer
	{
		private Int32 _targetFrame;

		public Int32 TargetFrame
		{
			get => _targetFrame;
			set
			{
				_targetFrame = value;
				PrepareStages();
			}
		}


		public Consoles.ConsoleType ConsoleType { get; set; }
		public List<TimeSpan> Stages { get; protected set; }


		public VariableTargetFrameTimer(Consoles.ConsoleType consoleType)
		{
			ConsoleType = consoleType;
			TargetFrame = -1;
			PrepareStages();
		}


		public Int32 Calibrate(Int32 hit) => 0;
		public Int32 GetMinutesBeforeTarget() => 0;


		public void Reset()
		{
			TargetFrame = -1;
			PrepareStages();
		}


		private void PrepareStages()
		{
			Stages = new List<TimeSpan>();
			for (var i = 0; GetStage(i) != Constants.NullTimeSpan; i++)
				Stages.Add(GetStage(i));
		}


		private TimeSpan GetStage(Int32 stage)
		{
			switch (stage)
			{
				case 0:
					return CalculateTarget();
				default:
					return Constants.NullTimeSpan;
			}
		}


		private TimeSpan CalculateTarget()
		{
			return _targetFrame == -1 ? Constants.InfiniteTimeSpan : new TimeSpan(0, 0, 0, 0, CalibrationHelper.ConvertToMillis(_targetFrame, ConsoleType));
		}
	}
}