﻿using System;
using EonTimer.Utilities.Constants;
using EonTimer.Utilities.Helpers;

namespace EonTimer.Timers
{
    public class EntralinkTimer : DelayTimer
    {
        public Int32 SecondaryCalibration { get; set; }

        public EntralinkTimer(Int32 calibration, Int32 secondaryCalibration, Int32 targetDelay, Int32 targetSecond, Consoles.ConsoleType consoleType, Int32 minLength, Boolean initialize = true)
            : base(calibration, targetDelay, targetSecond, consoleType, minLength, false)
        {
            SecondaryCalibration = secondaryCalibration;

            if (initialize)
                Initialize();
        }

        //overriden methods
        public new virtual Int32 Calibrate(Int32 result)
        {
            //convert to millis
            result = CalibrationHelper.ConvertToMillis(result, ConsoleType);
            var target = CalibrationHelper.ConvertToMillis(TargetDelay, ConsoleType);

            var offset = result - target;

            if (Math.Abs(offset) <= Constants.CloseThreshold)
                offset = (Int32)(Constants.CloseUpdateFactor * offset);
            else
                offset *= (Int32)Constants.UpdateFactor;

            return offset;
        }

        protected override TimeSpan GetStage(Int32 stage)
        {
            switch (stage)
            {
                case 0:
                    return base.GetStage(0).Add(new TimeSpan(0, 0, 0, 0, 250));
                case 1:
                    return base.GetStage(1).Subtract(new TimeSpan(0, 0, 0, 0, SecondaryCalibration));
                default:
                    return Constants.NullTimeSpan;
            }
        }
    }
}
