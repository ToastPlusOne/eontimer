﻿namespace EonTimer.Actions
{
	public interface ICountdownAction
	{
		void Action();
	}
}
